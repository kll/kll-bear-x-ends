// PRUSA iteration4
// X end idler
// GNU GPL v3
// Josef Průša <iam@josefprusa.cz> and contributors
// http://www.reprap.org/wiki/Prusa_Mendel
// http://prusamendel.org

use <MCAD/teardrop.scad>
use <config.scad>
use <util.scad>
use <x-end.scad>
use <x-end_idler-mount.scad>

module x_end_idler_holes()
{
  // idler mount
  translate([-getBearingToXAxisSpacing(), -20, getBeltPathOffset()])
  {
    x_end_idler_mount_cut();

    translate([0, 4, 0])
    {
      x_end_idler_mount_cut();
    }

    for (i=[-1,1])
    {
      translate([0, getYSize()/2 + 4, i*getXIdlerMountBoltSpacing()/2])
      {
        rotate([0, 0, 90])
        {
          flat_teardrop(getBoltDiameter()/2, 20, 90);
        }
      }
    }

    for (i=[-1,1])
    {
      translate([0, getYSize()/2 + getBoltCapThickness() + 8.5, i*getXIdlerMountBoltSpacing()/2])
      {
        rotate([0, 0, 90])
        {
          flat_teardrop(getBoltCapDiameter()/2, getBoltCapThickness(), 90);
        }
      }
    }

    *%x_end_idler_mount();
  }
}

module x_end_idler_base()
{
  difference()
  {
    x_end_plain();

    x_end_idler_holes();
  }

  //translate([-15,10.5,6]) rotate([90,0,0]) cylinder( h=13, r=6, $fn=30);
  //translate([-15,10.5,51]) rotate([90,0,0]) cylinder( h=13, r=6, $fn=30);

  /*difference()
  {
    translate(v=[-19,-16.5,30.25]) rotate(a=[0,-90,0]) cylinder(h = 1, r1=9, r2=12, $fn=30);
    translate([-25,-11.5,19]) cube([20,10,24]);
    translate(v=[0,-15.5,30.25]) rotate(a=[0,-90,0]) cylinder(h = 80, r=1.55, $fn=30);
  }*/
}

module selective_infill()
mirror([0,1,0]) translate([-50, -33, 0.6])
{
  difference()
  {
    union()
    {
      difference()
      {
        translate([50,50,0.6]) rotate([0,0,90]) cylinder( h=6, r=11.7, $fn=30);
        translate([50,50,-1]) rotate([0,0,90]) cylinder( h=10, r=11.5, $fn=30);
      }

      difference()
      {
        translate([50,50,0.6]) rotate([0,0,90]) cylinder( h=6, r=10.7, $fn=30);
        translate([50,50,-1]) rotate([0,0,90]) cylinder( h=10, r=10.5, $fn=30);
      }

      difference()
      {
        translate([50,50,0.6]) rotate([0,0,90]) cylinder( h=6, r=9.9, $fn=30);
        translate([50,50,-1]) rotate([0,0,90]) cylinder( h=10, r=9.7, $fn=30);
      }

      difference()
      {
        translate([50,50,0.6]) rotate([0,0,90]) cylinder( h=6, r=9, $fn=30);
        translate([50,50,-1]) rotate([0,0,90]) cylinder( h=10, r=8.8, $fn=30);
      }
    }

    translate([57.5,50.5,-1]) rotate([0,0,45]) cube([8,10,9]);
    translate([52,30.5,-1]) rotate([0,0,45]) cube([10,20,20]);
    translate([32,35.5,-1]) cube([8,30,9]);
  }
}

module x_end_idler()
{
  mirror([0,1,0])
  difference()
  {
    x_end_idler_base();

    // version
    translate([-getBackChunkXSize()/2 - getBearingToXAxisSpacing() - 0.1, -25, 2])
    {
      rotate([90,0,90])
      {
        linear_extrude(height = getExtrusionWidth() + 0.1)
        {
          text(getVersion(),font = "helvetica:style=Bold", size=4, halign="left");
        }
      }
    }
  }
}

  x_end_idler();
