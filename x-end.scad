// PRUSA iteration4
// X end prototype
// GNU GPL v3
// Josef Průša <iam@josefprusa.cz> and contributors
// http://www.reprap.org/wiki/Prusa_Mendel
// http://prusamendel.org

use <MCAD/linear_bearing.scad>
use <MCAD/polyholes.scad>
use <MCAD/teardrop.scad>
use <config.scad>
use <util.scad>
use <x-end_idler-mount.scad>

/*
 * Local parameters.
 */

// The distance between the rods on the X axis. Standard for i3 based designs is 45.
rod_distance = 45;

// The distance between the linear bearings and the threaded rod. Standard for i3 based designs is 17.
bearing_to_threaded_rod_spacing = 17;

// The distance between the center of the linear bearing and the front most edge. Limit for BNBSX extruder compatibility is 12.5.
bearing_to_front_edge_spacing = 12.5;

x_size = 38;

y_size = 39;

z_size = 58;

chamfer = 2;

bearing_chunk_wall_thickness = 5;

linear_bearing_diameter = linearBearing_D1("LM8UU");

// front_chunk_diameter =
//   max(
//     linear_bearing_diameter + bearing_chunk_wall_thickness,
//     getTrapezoidalNutHeadDiameter()
//   );
function getBackChunkXSize() = getXIdlerMountThickWidth() + 2*getExtrusionWidth();

function getXSize() = x_size;

function getYSize() = y_size;

function getZSize() = z_size;

//front_chunk_diameter = x_size - getBackChunkXSize();
front_chunk_diameter = 2*bearing_to_front_edge_spacing;

threaded_rod_chunk_height = getTrapezoidalNutTotalHeight() - getTrapezoidalNutHeadHeight();

threaded_rod_nut_trap_diameter = apo(getTrapezoidalNutMountSpacing()/2 + getSquareNutWidth()/2 + 2*getExtrusionWidth(), 6)*2;

// Final prototype
module x_end_plain()
{
  difference()
  {
    //x_end_base();
    x_end_body_base();
    x_end_holes();
  }

  // bearings stop
  bearings_stop_radius = apo(linear_bearing_diameter/2, 30);
  rotate([0, 0, 90])
  {
    difference()
    {
      translate([0, 0, z_size - 6*getLayerHeight()])
      {
        cylinder(h=6*getLayerHeight(), r=bearings_stop_radius, $fn=30);
      }

      translate([0, 0, z_size - 6*getLayerHeight() - 1])
      {
        cylinder( h=6*getLayerHeight() + 2, r=bearings_stop_radius - 2*getExtrusionWidth(), $fn=30);
      }

      translate([0, 0, z_size - 6*getLayerHeight()])
      {
        cylinder(h=4*getLayerHeight(), r1=bearings_stop_radius, r2=bearings_stop_radius - 2*getExtrusionWidth(), $fn=30);
      }

      rotate([0, 0, -90])
      {
        translate([0, -0.5, z_size - 6*getLayerHeight() - 1])
        {
          cube([linear_bearing_diameter, 1, 6*getLayerHeight() + 2]);
        }
      }
    }
  }
}

module x_end_body_base()
{
  // everything is centered on the linear bearing center
  bearing_offset = [-x_size/2 + bearing_to_front_edge_spacing, -y_size/4];

  // linear bearing chunk
  translate([0, 0, z_size/2])
  {
    chexagon(h=z_size, ds=front_chunk_diameter, c=chamfer, rotation=22.5);
  }

  // threaded rod nut chunk
  translate([0, -bearing_to_threaded_rod_spacing, threaded_rod_chunk_height/2])
  {
    chexagon(h=threaded_rod_chunk_height, ds=front_chunk_diameter, c=chamfer, rotation=22.5);
  }

  // back chunk
  back_chunk_x_offset = -(x_size - bearing_to_front_edge_spacing) + getBackChunkXSize()/2;
  back_chunk_y_offset = -y_size/2 + bearing_to_front_edge_spacing;
  translate([-getBearingToXAxisSpacing(), back_chunk_y_offset, z_size/2])
  {
    ccube([getBackChunkXSize(), y_size, z_size], chamfer);

    for(i=[-1, 1])
    {
      x_offset = getBackChunkXSize()/2;
      y_offset = -y_size/2 + 3 + sqrt(2);
      translate([i*x_offset, y_offset, 0])
      {
        ccube([2*sqrt(2+.01), 6, getXIdlerMountHeight()], 2, sides=true);
      }
    }
  }
}

module threaded_rod_chunk()
{
  translate([0, -bearing_to_threaded_rod_spacing, 0])
  {
    rotate([0, 0, 22.5])
    {
      cylinder(h = threaded_rod_chunk_height, d=front_chunk_diameter, $fn=100);
    }

    //translate([0, 0, threaded_rod_chunk_height/2])
    //ccube([front_chunk_diameter, front_chunk_diameter, threaded_rod_chunk_height], 2);

    translate([0, 0, -1]) cylinder(h = z_size + 2, d=8);
  }
}

module vertical_bearing_holes()
{
  // bearing shaft
  translate([0, 0, -1])
  {
    polyhole(h=z_size + 2, d=linear_bearing_diameter);
  }

  // tapered opening to bearing shaft
  translate([0, 0, -0.1])
  {
    cylinder(r1=(linear_bearing_diameter/2) + 0.7, r2=(linear_bearing_diameter/2), h=0.5);
  }

  // cut down side to allow flex
  rotate([0, 0, 0]) // original -40
  {
    translate([0, -0.5, -1])
    {
      cube([50, 1, z_size + 2]);
    }
  }
}

module x_end_holes()
{
  vertical_bearing_holes();

  /*// Belt hole
  translate(v=[-1,0,0])
  {
    // Stress relief
    translate(v=[-5.5-10+1.5,-10-1,30]) cube(size = [18,1,28], center = true);
    difference()
    {
      translate(v=[-5.5-10+1.5,-10,30]) cube(size = [10,46,28], center = true);

      // Nice edges
      translate(v=[-5.5-10+1.5-5,-10,30+23]) rotate([0,20,0]) cube(size = [10,46,28], center = true);
      translate(v=[-5.5-10+1.5+5,-10,30+23]) rotate([0,-20,0]) cube(size = [10,46,28], center = true);
      translate(v=[-5.5-10+1.5,-10,30-23]) rotate([0,45,0]) cube(size = [10,46,28], center = true);
      translate(v=[-5.5-10+1.5,-10,30-23]) rotate([0,-45,0]) cube(size = [10,46,28], center = true);
    }
  }*/

  // pushfit rods
  translate([-getBearingToXAxisSpacing(),-26,6])
  {
    rotate([0, 0, 90])
    {
      for(i=[0, 1])
      {
        translate([0, 0, i*rod_distance])
        {
          pushfit_rod(50);
        }
      }
    }
  }

  translate([-getBearingToXAxisSpacing(),0,6])
  {
    rotate([0, 0, 90])
    {
      for(i=[0, 1])
      {
        translate([0, 0, i*rod_distance])
        {
          rotate([0, 90, 0])
          {
            polyhole(d=getBoltDiameter(), h=y_size, center=true);
          }
        }
      }
    }
  }

  waste_pocket();

  // Thread rod nut
  translate(v=[0,-bearing_to_threaded_rod_spacing, 0])
  {
    rotate([0, 0, 45]) // z -135
    {
      tr_nut_mount();
    }
  }
}

module pushfit_rod(length)
{
  /*polyhole(h = length, r=diameter/2);
  difference()
  {
    translate(v=[0,-diameter/2.85,length/2]) rotate([0,0,45]) cube(size = [diameter/2,diameter/2,length], center = true);      translate(v=[0,-diameter/4-diameter/2-0.4,length/2]) rotate([0,0,0]) cube(size = [diameter,diameter/2,length], center = true);
  }*/
  diameter = getPushfitRodDiameter() + getTolerance();
  flat_teardrop(diameter/2, length, 90);
}

module waste_pocket()
{
  // waste pocket
  translate([-getBearingToXAxisSpacing(),-1,6]) rotate([90,0,0]) cylinder( h=5, r=5, $fn=30);
  translate([-getBearingToXAxisSpacing(),-1,51]) rotate([90,0,0]) cylinder( h=5, r=5, $fn=30);
  translate([-getBearingToXAxisSpacing(),-5.9,6]) rotate([90,0,0]) cylinder( h=3, r1=5, r2=4.3, $fn=30);
  translate([-getBearingToXAxisSpacing(),-5.9,51]) rotate([90,0,0]) cylinder( h=3, r1=5, r2=4.3, $fn=30);

  // opening window
  translate([-17,-1,z_size-3]) rotate([90,0,0]) cube([4,4,4]);
  translate([-17,-1,-1]) rotate([90,0,0]) cube([4,4,4]);
}

module tr_nut_mount()
{
  screw_offset = getTrapezoidalNutMountSpacing()/2;
  nut_trap_length = screw_offset+getSquareNutWidth()/2;
  nut_trap_z_offset = 2;
  tr_nut_cut_radius = apo(r=getTrapezoidalNutHoleDiameter()/2, n=6);
  lower_screw_cut_length = nut_trap_z_offset + getSquareNutThickness();
  upper_screw_cut_length = 4;
  upper_screw_cut_z_offset = nut_trap_z_offset + getSquareNutThickness() + getLayerHeight();

  // TR nut cutout
  translate([0, 0, -1])
  cylinder(h = threaded_rod_chunk_height + 2, r = tr_nut_cut_radius, $fn = 6);

  // cut all the way up to ensure clearence around the threaded rod
  *translate([0, 0, threaded_rod_chunk_height])
  {
    cylinder(h = z_size - threaded_rod_chunk_height, r1 = tr_nut_cut_radius, r2=10/2, $fn = 6);

  }

  // visual aid threaded rod
  %cylinder(h=z_size + 2, d=8);

  for (y=[-1,1])
  {
    // screw hole
    translate([0, y*screw_offset, -1])
    {
      // hole up to top side of square nut
      polyhole(lower_screw_cut_length, getBoltDiameter(), $fn=50);

      // hole above square nut, leaving thin support layer that must be drilled out
      translate([0, 0, upper_screw_cut_z_offset + 1])
      {
        polyhole(upper_screw_cut_length, getBoltDiameter());
      }
    }

    // square nut trap
    rotate([0, 0, y*90])
    {
      translate([0, -getSquareNutWidth()/2, nut_trap_z_offset])
      {
        cube([nut_trap_length, getSquareNutWidth(), getSquareNutThickness()]);
      }
    }

    // visual aid TR nut
    *%translate([0, 0, -getTrapezoidalNutHeadHeight()])
    {
      cylinder(d=getTrapezoidalNutHeadDiameter(), h=getTrapezoidalNutHeadHeight());
      cylinder(d=getTrapezoidalNutHoleDiameter(), h=getTrapezoidalNutTotalHeight());
    }
  }
}

module bounding_box()
{
  // size limit bounding box for compatibility with BNBSX extruder
  translate([-26.5, -29.5, 0])
  {
    difference()
    {
      translate([-10, -10, -10])
      {
        cube([58.9, 60, 78]);
      }

      cube([38.9, 40, z_size]);

      // visual aid
      %cube([38.9, 40, z_size]);
    }
  }
}

//difference ()
//{
  x_end_plain();
//  bounding_box();
//}
